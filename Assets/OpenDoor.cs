using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
   float speed =100f;
   int angle = 90;
   AudioSource audioSource;
   [SerializeField] AudioClip doorSound;
   bool audioPlayed;
    void Start()
    {
         audioSource = GetComponent<AudioSource>();
         audioPlayed = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(!audioSource.isPlaying && !audioPlayed)
           
        {
              audioSource.PlayOneShot(doorSound); 
              audioPlayed = true;
        }
          
    }
    private void OnTriggerStay(Collider other) 
    {
        if (other.tag == "Hand")
        {
            if (transform.rotation.eulerAngles.y  <= 180)
            {
                transform.Rotate(Vector3.up * speed * Time.deltaTime);
                Debug.Log("hit the door");
                
                //angle ++;
            }
        }
    }
  
}
