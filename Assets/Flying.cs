using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flying : MonoBehaviour
{
    float speed =50f;
    public Transform target;
    new Vector3 move = new Vector3(84.5899963f,42.7999992f,144.393906f);
    Rigidbody rb;
    public GameObject broom;
    AudioSource audioSource;
    [SerializeField] AudioClip fly;
    bool audioPlayed;
    private void OnTriggerEnter(Collider other)
    {
        
        if(!audioSource.isPlaying && !audioPlayed)
           
        {
            audioSource.PlayOneShot(fly); 
            audioPlayed = true;
        }
    }
    private void OnTriggerStay(Collider other) 
    { 
        GameObject player = GameObject.Find("XR Origin");
         rb = player.GetComponent<Rigidbody>();
         
      //  player.GetComponent<Rigidbody>().useGravity = false;
        //player.transform.position(Vector3.up * speed * Time.deltaTime);
        float step = speed * Time.deltaTime;
        //player.transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        
  //  move = new Vector3(84.5899963,42.7999992,144.393906);
        //  player.transform.Translate(move * speed * Time.deltaTime);
        rb.AddRelativeForce(Vector3.up * speed * Time.deltaTime);
        broom = GameObject.FindWithTag("grabbedBroom");
        if(broom != null)
            broom.gameObject.tag = "Flying";


    }
    private  void OnTriggerExit(Collider other) 
    {
        GameObject player = GameObject.Find("XR Origin");
        rb = player.GetComponent<Rigidbody>(); 
        rb.useGravity = false;
        rb.freezeRotation = true;
        rb.velocity = new Vector3(0, 0, 0);
        
        
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioPlayed = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
