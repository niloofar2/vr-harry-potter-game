using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Grabbing : MonoBehaviour
{
    // // Start is called before the first frame update
    // bool collect = false;
    // new Vector3 leftHandPosition= new Vector3( 0 , 0  , 0);
    // new Vector3 rightHandPosition = new Vector3( 0 , 0  , 0);

    // private void OnTriggerStay(Collider other) 
    // {     
            
    //     if (other.gameObject.name == "Wand" )
    //      { 
    //         collect = true;
    //         Debug.Log("grabbed");
    //      }
    //       GameObject rightHand = GameObject.Find("RightHand Controller");
    //       rightHandPosition = rightHand.transform.position;
    //       GameObject leftHand = GameObject.Find("LeftHand Controller");
    //       leftHandPosition = leftHand.transform.position;
       
    //     // if (Input.GetButton("LeftTrigger") && Input.GetButton("RightTrigger") && collect)
    //     // {
    //     //   Debug.Log("bothHands");
    //     //   other.transform.position = (leftHandPosition  + rightHandPosition)/2f; 
    //     //   other.transform.rotation= Quaternion.LookRotation((leftHandPosition- rightHandPosition), Vector3.up);

    //     // }
    //    if(Input.GetButton("LeftTrigger"))
    //     { 
    //       other.transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
    //       other.transform.position = leftHandPosition;
        
    //     }
    //   else if (Input.GetButton("RightTrigger"))
    //     {
          
        
    //        other.transform.rotation =rightHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
    //         other.transform.position = rightHandPosition;
        
    //     }
      

        
    //     collect = false;
    //}





    bool collect = false;
    new Vector3 leftHandPosition= new Vector3( 0 , 0  , 0);
    new Vector3 rightHandPosition = new Vector3( 0 , 0  , 0);

    private void OnTriggerStay(Collider other) 
    {     
            
        if (other.gameObject.name == "Magnifying Glass" || other.gameObject.name == "Cube" )
          collect = true;
          GameObject rightHand = GameObject.Find("RightHand Controller");
          rightHandPosition = rightHand.transform.position;
          GameObject leftHand = GameObject.Find("LeftHand Controller");
          leftHandPosition = leftHand.transform.position;
       
        if (Input.GetButton("LeftTrigger") && Input.GetButton("RightTrigger") && collect)
        {
          Debug.Log("bothHands");
          other.transform.position = (leftHandPosition  + rightHandPosition)/2f; 
          other.transform.rotation= Quaternion.LookRotation((leftHandPosition- rightHandPosition), Vector3.up);

        }
        else if(Input.GetButton("LeftTrigger") && collect)
        { 
          other.transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
          other.transform.position = leftHandPosition;
        
        }
      else if (Input.GetButton("RightTrigger") && collect)
        {
          
        
           other.transform.rotation =rightHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
            other.transform.position = rightHandPosition;
        
        }
      

        
        collect = false;
    }
}
