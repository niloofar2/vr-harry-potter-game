using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class accio : MonoBehaviour
{
    new Vector3 leftHandPosition= new Vector3( 0 , 0  , 0);
    new Vector3 rightHandPosition = new Vector3( 0 , 0  , 0);
    new Vector3 playerPosition = new Vector3( 0 , 0  , 0);
    public GameObject broom;
    AudioSource audioSource;
    [SerializeField] AudioClip accioSpell;
   // [SerializeField] AudioClip fly;
    //[SerializeField] AudioClip alohomoraSpell;
    //[SerializeField] AudioClip lumosSpell;
    
    [SerializeField] public float x = -0.035f;
    [SerializeField] public float y = -0.31f;
    [SerializeField] public float z = -0.78f;
    bool check = true;
    public Transform broomTransform;
    //public Transform targetTransform;
    public void Accio()
    {
        GameObject rightHand = GameObject.Find("RightHand Controller");
        rightHandPosition = rightHand.transform.position;
        GameObject leftHand = GameObject.Find("LeftHand Controller");
        leftHandPosition = leftHand.transform.position;
        broom = GameObject.FindWithTag("Broom");
        if(broom != null)
        {
           
            // broom.transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
            // broom.transform.position = leftHandPosition + new Vector3(x, y , z);
            broom.gameObject.tag = "grabbedBroom";
             audioSource.PlayOneShot(accioSpell);
             Vector3 pivotOffset = leftHandPosition - 
             broomTransform.TransformPoint(broomTransform.GetComponent<MeshFilter>().sharedMesh.bounds.center);
        broomTransform.position = leftHandPosition - pivotOffset;
        broomTransform.rotation = leftHand.transform.rotation;
           


        }
    }
    // private void OnTriggerStay(Collider other) 
    // {
    //      GameObject rightHand = GameObject.Find("RightHand Controller");
    //     rightHandPosition = rightHand.transform.position;
    //     GameObject leftHand = GameObject.Find("LeftHand Controller");
    //     leftHandPosition = leftHand.transform.position;
    //     if (other.gameObject.name == "LeftHand Controller")
    //     {
    //         //transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
    //         transform.position = leftHand.transform.position;
    //     }
        
    // }
    private void Start() {
         audioSource = GetComponent<AudioSource>();
    }
    private void Update() {
        
        GameObject rightHand = GameObject.Find("RightHand Controller");
        rightHandPosition = rightHand.transform.position;
        GameObject leftHand = GameObject.Find("LeftHand Controller");
        leftHandPosition = leftHand.transform.position;
        broom = GameObject.FindWithTag("grabbedBroom");
        if(broom != null)
        {
           
            broom.transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
            broom.transform.position = leftHandPosition + new Vector3(x, y , z);
           


        }
        GameObject player = GameObject.Find("Main Camera");
        playerPosition = player.transform.position;
        broom = GameObject.FindWithTag("Flying");
        if(broom != null)
        {
           
            ///broom.transform.rotation =player.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
           /// broom.transform.rotation = Quaternion.AngleAxis(0, Vector3.left);

           //broom.transform.Rotate(Vector3.up * Time.deltaTime,  Space.Self);
           broom.GetComponent<Rigidbody>().useGravity = false;
          // broom.transform.rotation= Quaternion.LookRotation(playerPosition, Vector3.left);
          //  broom.transform.Rotate(Vector3.left, 0f);
         // broom.transform.rotation = Quaternion.LookRotation(playerPosition, transform.up);
    //         Vector3 eulers = broom.transform.eulerAngles;
    //  eulers.y = 0;
    //  broom.transform.eulerAngles = eulers;
          
            broom.transform.position = playerPosition + new Vector3(0, -1f , 0);
            float currentXRotation = broom.transform.rotation.eulerAngles.x;
            Quaternion newRotation = Quaternion.Euler(currentXRotation, 90f , 90f) * 
            Quaternion.Euler(0f , player.transform.rotation.eulerAngles.y, player.transform.rotation.eulerAngles.z);

// Set the rotation of the broom object to the new rotation
            broom.transform.rotation = newRotation;
           


        }
    }

}
