using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Land : MonoBehaviour
{
    float speed =50f;
    Rigidbody rb;
    public GameObject broom;
    private void OnTriggerStay(Collider other) 
    { 
        GameObject player = GameObject.Find("XR Origin");
        rb = player.GetComponent<Rigidbody>();
        float step = speed * Time.deltaTime;
        rb.AddRelativeForce(-Vector3.up * speed * Time.deltaTime);
       // rb.useGravity = true;
        broom.SetActive(false);
    
        


    }
     private void OnTriggerExit(Collider other) 
    { 
        GameObject player = GameObject.Find("XR Origin");
        rb = player.GetComponent<Rigidbody>();
        rb.useGravity = true;
      
    
        


    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
