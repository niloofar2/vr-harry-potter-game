using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grabMatchbox : MonoBehaviour
{
   new Vector3 leftHandPosition= new Vector3( 0 , 0  , 0);
    new Vector3 rightHandPosition = new Vector3( 0 , 0  , 0);
    new Vector3 playerPosition = new Vector3( 0 , 0  , 0);
     private void OnTriggerStay(Collider other) 
     {
        if (other.gameObject.name == "LeftHand Controller")
        { 
            gameObject.transform.tag = "grabMatchbox";
            Debug.Log("grabbed matchbox");
        
        }
     }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("LeftTrigger"))
       {
         gameObject.transform.tag = "Untagged";
       }
        if (gameObject.tag == "grabMatchbox")
        {
           GameObject rightHand = GameObject.Find("RightHand Controller");
        rightHandPosition = rightHand.transform.position;
        GameObject leftHand = GameObject.Find("LeftHand Controller");
        leftHandPosition = leftHand.transform.position;
        
           
            gameObject.transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(0,90, 0));
            gameObject.transform.position = leftHandPosition;
           


        
        }
    }
}
