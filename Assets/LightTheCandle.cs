using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTheCandle : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject candleLight;
    [SerializeField] GameObject particles;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MatchOn" || other.tag == "OnGrounOn")// my mistake : I also put other.name == "Match"  so if I drop the match it does not light the candle
        {
            Debug.Log("the match is hitting the candle");
            candleLight.SetActive(true);
            particles.SetActive(true);

        }
    }
}
