using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flyCandle : MonoBehaviour
{
    new Vector3 leftHandPosition= new Vector3( 0 , 0  , 0);
    new Vector3 rightHandPosition = new Vector3( 0 , 0  , 0);
    new Vector3 playerPosition = new Vector3( 0 , 0  , 0);
    [SerializeField] public float x = 1f;
    [SerializeField] public float y = 1f;
    [SerializeField] public float z =1f;
    public GameObject candle;
    AudioSource audioSource;
    [SerializeField] AudioClip accioSpell;
    public void WingardiumLeviosa()
    {
        GameObject rightHand = GameObject.Find("RightHand Controller");
        rightHandPosition = rightHand.transform.position;
        GameObject leftHand = GameObject.Find("LeftHand Controller");
        leftHandPosition = leftHand.transform.position;
        
           
            gameObject.transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(0,0, 0));
            gameObject.transform.position = leftHandPosition + new Vector3(x, y , z);
            gameObject.tag = "flyingCandle";
             //audioSource.PlayOneShot(accioSpell);
        
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         GameObject rightHand = GameObject.Find("RightHand Controller");
        rightHandPosition = rightHand.transform.position;
        GameObject leftHand = GameObject.Find("LeftHand Controller");
        leftHandPosition = leftHand.transform.position;
        if (gameObject.tag == "flyingCandle")
      
        {
           
            gameObject.transform.rotation =leftHand.transform.rotation * Quaternion.Euler(new Vector3(90,0, 0));
            gameObject.transform.position = leftHandPosition + new Vector3(x, y , z);
           


        }
        

    }
}
